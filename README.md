# Agenda culturel participatif

## Installation

### Needed dependencies

- `nodejs` > 18
- `yarn`
- Mariadb/Mysql

### Start the project

- Clone this repository
- Create a `.env` at the root of the folder:

```
cp .env.example .env
```

- Install project dependencies:

```
yarn
```

- Start the server:

```
yarn dev
```

### Database

- Edit the `.env` file with your database information:

```
DB_HOST=127.0.0.1
DB_PORT=3306
DB_NAME=Name of your database
DB_USER=Your user
DB_SECRET=Your password
```
