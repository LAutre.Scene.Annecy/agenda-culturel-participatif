import { Router } from 'express';
import routes from './v1/index.js';

const router = Router();

router.get('/', (req, res) => {
  res.send('🔥 Hic sunt dracones 🔥');
});

// Route groups
router.use('/v1', routes);

export default router;
